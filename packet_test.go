package sauerproto

import (
	"testing"
)

func TestGet(t *testing.T) {
	data := []byte{1, 2, 3}
	p := New(data)
	for i := 1; i < 4; i++ {
		b, err := p.Get()
		if err != nil {
			t.Errorf("read from packet: %v", err)
		}

		if int(b) != i {
			t.Errorf("expected %d got %d", i, b)
		}
	}
}

func TestGetInt(t *testing.T) {
	expected := []int{0x00ba, 0xddccbbaa}
	data := []byte{0x80, 0xba, 0, 0x81, 0xaa, 0xbb, 0xcc, 0xdd}
	p := New(data)

	for _, v := range expected {
		c, err := p.GetInt()
		if err != nil {
			t.Errorf("read from packet: %v", err)
		}

		if c != v {
			t.Errorf("expected %d got %d", v, c)
		}
	}
}

func TestGetString(t *testing.T) {
	data := []byte{215, 112, 234, 229, 101, 241, 0, 123, 123}
	expected := "Пpивeт"
	p := New(data)
	s, err := p.GetString()

	if err != nil {
		t.Errorf("testing GetString: %v", err)
	}

	if s != expected {
		t.Errorf("testing GetString: expected %q got %q", expected, s)
	}
}
