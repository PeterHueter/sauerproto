package sauerproto

import (
	"bytes"
	"fmt"

	"bitbucket.org/PeterHueter/sauerproto/unicode"
)

// Reader is packet interface
type Reader interface {
	Get() (byte, error)
	GetInt() (int, error)
	GetString() (string, error)
}

// Writer is packet writer interface
type Writer interface {
	Put(byte) error
	PutInt(int) error
	PutString(string) error
}

// Packet is represend sauer protocol packet
type Packet struct {
	buf *bytes.Buffer
}

// New create sauer packet from bytes
func New(data []byte) *Packet {
	return &Packet{
		bytes.NewBuffer(data),
	}
}

// GetInt read bytes from packet and unpack into integer
// or return io.EOF
func (p *Packet) GetInt() (int, error) {
	b, err := p.Get()
	if err != nil {
		return 0, err
	}

	result := int(b)
	switch b {
	// 2 bytes
	case 0x80:
		return p.getN(2)

	// 4 bytes
	case 0x81:
		return p.getN(4)
	}
	return result, nil
}

// Get read single byte or return io.EOF
func (p *Packet) Get() (byte, error) {
	return p.buf.ReadByte()
}

// getN read n bytes if avilable and pack into integer
func (p *Packet) getN(n int) (int, error) {
	var result int
	for i := 0; i < n; i++ {
		b, err := p.buf.ReadByte()
		if err != nil {
			return 0, fmt.Errorf("read n bytes: %v", err)
		}
		result |= int(b) << (uint(i) * 8)
	}

	return int(result), nil
}

// GetString read string from packet and convert to unicode
func (p *Packet) GetString() (string, error) {
	var err error
	var b byte
	r := make([]rune, 0, 16)

	for b, err = p.Get(); b != 0 && err == nil; b, err = p.Get() {
		r = append(r, unicode.Cube2rune(b))
	}

	if err != nil {
		return "", fmt.Errorf("read string: %v", err)
	}
	return string(r), nil
}
