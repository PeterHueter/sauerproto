package sauerproto

const (
	EXT_ACK                    = -1
	EXT_VERSION                = 105
	EXT_NO_ERROR               = 0
	EXT_ERROR                  = 1
	EXT_PLAYERSTATS_RESP_IDS   = -10
	EXT_PLAYERSTATS_RESP_STATS = -11
	EXT_UPTIME                 = 0
	EXT_PLAYERSTATS            = 1
	EXT_TEAMSCORE              = 2
)
