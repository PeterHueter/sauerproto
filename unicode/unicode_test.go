package unicode

import (
	"testing"
)

func TestGet(t *testing.T) {
	sauer := [...]byte{215, 112, 234, 229, 101, 241}
	normal := "Пpивeт"
	var w string
	for _, b := range sauer {
		w += string(Cube2rune(b))
	}
	if w != normal {
		t.Errorf("convert to unicode: expected %s got %s", normal, w)
	}
}
